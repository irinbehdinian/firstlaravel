<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/posts.create','PostsController@create')->name('create12-post');
Route::post('/posts/create','PostsController@store')->name('store-post');
Route::get('/posts/{post}', 'PostsController@show')->name('show-post');
Route::get('/posts', 'PostsController@index')->name('index-post');
Route::get('/posts/{post}/edit', 'PostsController@edit')->name('edit-post');
Route::patch('/posts/{post}', 'PostsController@update')->name('update-post');
Route::delete('/posts/{post}', 'PostsController@destroy')->name('delete-post');

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


