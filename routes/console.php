<?php

use Illuminate\Foundation\Inspiring;
use App\Permission;
use App\Role;
use App\User;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('makeRoles', function () {
    $mainAccess = new Permission();
    $mainAccess->name = 'adminAccess';
    $mainAccess->save();

    $ownOrders = new Permission();
    $ownOrders->name = 'ownOrders';
    $ownOrders->save();

    $editOrders = new Permission();
    $editOrders->name = 'editOrders';
    $editOrders->save();

    $ownBmi = new Permission();
    $ownBmi->name = 'ownBmi';
    $ownBmi->save();


    $bmiForm = new Permission();
    $bmiForm->name = 'bmiForm';
    $bmiForm->save();

    $viewReport = new Permission();
    $viewReport->name = 'viewReport';
    $viewReport->save();

    $transaction = new Permission();
    $transaction->name = 'transaction';
    $transaction->save();

    $plateTable = new Permission();
    $plateTable->name = 'plateTable';
    $plateTable->save();

    $orderTable = new Permission();
    $orderTable->name = 'orderTable';
    $orderTable->save();

    $productTable = new Permission();
    $productTable->name = 'productTable';
    $productTable->save();

    $editProduct = new Permission();
    $editProduct->name = 'editProduct';
    $editProduct->save();

    $package = new Permission();
    $package->name = 'package';
    $package->save();

    $subscribe = new Permission();
    $subscribe->name = 'subscribe';
    $subscribe->save();

    $admin = new Role();
    $admin->name = 'admin';
    $admin->save();
    $user = new Role();
    $user->name = 'user';
    $user->save();
    $chef = new Role();
    $chef->name = 'chef';
    $chef->save();
    $operator = new Role();
    $operator->name = 'operator';
    $operator->save();
    $admin->attachPermissions(array($mainAccess,$ownOrders,$editOrders,$ownBmi,$bmiForm,
        $viewReport,$plateTable,$orderTable,$productTable,$editProduct,$package,$subscribe,$transaction));
    $user->attachPermissions(array($ownOrders, $ownBmi,$subscribe,$transaction));
    $chef->attachPermissions(array($ownOrders,$editOrders,$ownBmi,$bmiForm,
        $viewReport,$plateTable,$orderTable,$productTable,$editProduct,$package,$subscribe));
    $user = User::findOrFail(1);
    $user->attachRole($admin);
})->describe('make all roles and admin');




Artisan::command('operator', function () {

    $user = new User();
    $user->name = 'op';
    $user->email = 'operator@operator.ir';
    $user->password = bcrypt('123qweR');
    $user->save();

    $owner = new Role();
    $owner->name         = 'postoperator';
    $owner->display_name = 'Post operator'; // optional
    $owner->description  = 'Post operator'; // optional
    $owner->save();

    $user->attachRole($owner);

    $createPost = new Permission();
    $createPost->name         = 'create-post';
    $createPost->display_name = 'Create Posts'; // optional
// Allow a user to...
    $createPost->description  = 'create new blog posts'; // optional
    $createPost->save();


    $owner->attachPermission($createPost);

})->describe('operator role');